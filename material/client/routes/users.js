/**
 * Created by smjain on 23/2/17.
 */
var express       = require('express'),
    router        = express.Router(),
    mongoose      = require('mongoose'),
    path          = require('path'),
    Grid          = require('gridfs-stream'),
    Users       = require('../models/users');

router.callUsers = function() {
    Grid.mongo = mongoose.mongo;
    /*User's _id, fn, ln, email, and phone number*/
    Users.find({}, function (err, files) {
        if (err) {
            console.log(err);
        }
        if (files) {
            var user = files;
            for (var i=0; i<= user.length; i++) {
                console.log("user "+user);
            }
        }
    });
};

module.exports = router;