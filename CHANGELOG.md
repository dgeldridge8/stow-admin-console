### Changelog

**VERSION 1.2.0** (01-10-2017)

    - Feat: Upgrade to Bootstrap 4!
    - Feat: Upgrade angular to 1.6.x 
    - Feat: Upgrade jQuery to 3.1.x
    - Feat: Upgrade material-design-lite to 1.3.x
    - Fix: Fix "angular-material datepicker goes blank after upgrade Angular to 1.6.x" problem
    - Chore: Clean up no longer used style files


**VERSION 1.1.1** (12-28-2016)

    - Feat: Upgrade Bower packages to latest version, including angular-translate (2.13.x), font-awesome (4.7.x), angular-wizard (0.10.x)
    - Feat: New profile page
    - Fix: Fix links and sidebar link highlight for Angular 1.6
    - Chore(Icon Page): Change material icon source link to new one
    - Chore: Clean up


**VERSION 1.1.0** (10-07-2016)

    - Feat: Upgrade angular-material to 1.1.x
    - Feat: Upgrade angular-wizard to 0.9.x
    - Feat: Upgrade material icons to 3.0.x
    - Feat: Upgrade other Bower packages to latest version 
    - Feat: Upgrade NPM packages to latest version
    - Fix: Fix material dialog is hidden when screen resolution is < 997px bug, thanks for the feedback @barbaray_yann 


**VERSION 1.0.4** (08-16-2016)

    - Feat: New landing page!


**VERSION 1.0.3** (08-13-2016)

    - Fix(Search Overlay): Fix placeholder text alignment problem
    - Fix(Safari): Fix sidebar fail to show up problem
    - Feat(Page Layout): New page with tabs template
    - Feat(UI): New left side only timeline
    - Chore(i18n): Update i18n data


**VERSION 1.0.2** (08-09-2016)

    - Feat: Upgrade Bootstrap to 3.3.7
    - Feat(UI): New Feature callout component
    - Feat(UI): New Call-to-Action component
    - Feat(UI): New Sash component
    - Feat(UI): Add boxed icon boxes style
    - Feat(UI): New testimonials component
    - Feat(UI): New classic style hover
    - Feat(UI): Add Icon boxes
    - Feat(UI): New colors for cards
    - Feat(Page): New about page
    - Feat(Page): New contact page
    - Feat(Page): New services page
    - Feat(Page): New maintenance page
    - Feat(Page): New careers page
    - Feat(Page): New blog page
    - Feat(eCommerce): New products page
    - Feat(Layout): Add blank page & no sidebar page
    - Chore(i18n): Update translation data


**VERSION 1.0.0** (08-09-2016)

    - Initial release
